#include<stdio.h>
int input()
{
    int n;
    printf("Enter integer value: \n");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int temp,sum=0;
    while(n!=0)
    {
        temp=n%10;
        sum=sum+temp;
        n=n/10;
    }
    return sum;
}
void output(int sum)
{
    printf("The sum of digits = %d",sum);
}
void main()
{
    int n,sum;
    n = input();
    sum = compute(n);
    output(sum);
}